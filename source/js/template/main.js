(function () {
	"use strict";

	/*------ ScrollUp -------- */
	jQuery.scrollUp({
		scrollText: '<i class="icon-arrow-up"></i>',
		easingType: 'linear',
		scrollSpeed: 900,
		animation: 'fade'
	});

	/*------ Wow Active ----*/
	new WOW().init();

	/*--------------------------------
	InstagramFeed active
	-----------------------------------*/

	jQuery(window).on('load', function(){
		var instagramFeedSliderOne = function () {
			jQuery.instagramFeed({
			username: "ecommerce.devitems",
			container: "#instagramFeedOne",
			display_profile: false,
			'display_biography': false,
			display_gallery: true,
			callback: null,
			styling: false,
			items: 8,
			});
		};
		instagramFeedSliderOne();
		jQuery("#instagramFeedOne").on("DOMNodeInserted", function (e) {
			if (e.target.className === "instagram_gallery") {
				jQuery(".instagram-carousel .instagram_gallery").slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					autoplay: false,
					dots: false,
					arrows: false,
					loop: true,
					responsive: [
						{
						breakpoint: 767,
						settings: {
								slidesToShow: 3,
							}
						},
						{
						breakpoint: 575,
						settings: {
								slidesToShow: 2,
							}
						}
					]
				});
				jQuery(".instagram-carousel-2 .instagram_gallery").slick({
					slidesToShow: 8,
					slidesToScroll: 1,
					autoplay: false,
					dots: false,
					arrows: false,
					loop: true,
					responsive: [
						{
						breakpoint: 1199,
						settings: {
								slidesToShow: 6,
							}
						},
						{
						breakpoint: 991,
						settings: {
								slidesToShow: 5,
							}
						},
						{
						breakpoint: 767,
						settings: {
								slidesToShow: 3,
							}
						},
						{
						breakpoint: 575,
						settings: {
								slidesToShow: 2,
							}
						}
					]
				});
			}
		});

	});

	/*--- Language currency active ----*/
	// $('.language-dropdown-active').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('.language-dropdown').slideToggle(400);
	// });
	// $('.currency-dropdown-active').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('.currency-dropdown').slideToggle(400);
	// });

	/*--- Countdown timer active ----*/
	// $('#timer-1-active , #timer-3-active').syotimer({
	// 	year: 2020,
	// 	month: 10,
	// 	day: 22,
	// 	hour: 8,
	// 	minute: 48,
	// 	layout: 'hms',
	// 	periodic: false,
	// 	periodUnit: 'm'
	// });
	//
	// $('#timer-2-active').syotimer({
	// 	year: 2020,
	// 	month: 10,
	// 	day: 22,
	// 	hour: 8,
	// 	minute: 48,
	// 	layout: 'dhms',
	// 	periodic: false,
	// 	periodUnit: 'm'
	// });

	/*====== SidebarCart ======*/
	function miniCart() {
		var navbarTrigger = jQuery('.cart-active'),
		endTrigger = jQuery('.cart-close'),
		container = jQuery('.sidebar-cart-active'),
		wrapper = jQuery('.main-wrapper');

		wrapper.prepend('<div class="body-overlay"></div>');

		navbarTrigger.on('click', function(e) {
			e.preventDefault();
			container.addClass('inside');
			wrapper.addClass('overlay-active');
		});

		endTrigger.on('click', function() {
			container.removeClass('inside');
			wrapper.removeClass('overlay-active');
		});

		jQuery('.body-overlay').on('click', function() {
			container.removeClass('inside');
			wrapper.removeClass('overlay-active');
		});
	};
	miniCart();

	/*-------------------------------
	Header Search Toggle
	-----------------------------------*/
	var searchToggle = jQuery('.search-toggle');
	searchToggle.on('click', function(e){
		e.preventDefault();
		if(jQuery(this).hasClass('open')){
			jQuery(this).removeClass('open');
			jQuery(this).siblings('.search-wrap-1').removeClass('open');
		}else{
			jQuery(this).addClass('open');
			jQuery(this).siblings('.search-wrap-1').addClass('open');
		}
	})


	/* NiceSelect */
	// jQuery('.nice-select').niceSelect();


	/*-------------------------
	Category active
	--------------------------*/
	jQuery('.categori-show').on('click', function(e) {
		e.preventDefault();
		jQuery('.categori-hide , .categori-hide-2').slideToggle(900);
	});



	/*------- Color active -----*/
	jQuery('.pro-details-color-content').on('click', 'a', function(e){
		e.preventDefault();
		jQuery(this).addClass('active').parent().siblings().children('a').removeClass('active');
	});

	/*--------------------------------
	Social icon active
	-----------------------------------*/
	if (jQuery('.pro-details-action').length) {
		var $body = jQuery('body'),
		$cartWrap = jQuery('.pro-details-action'),
		$cartContent = $cartWrap.find('.product-dec-social');
		$cartWrap.on('click', '.social', function(e) {
			e.preventDefault();
			var $this = jQuery(this);
			if (!$this.parent().hasClass('show')) {
				$this.siblings('.product-dec-social').addClass('show').parent().addClass('show');
			} else {
				$this.siblings('.product-dec-social').removeClass('show').parent().removeClass('show');
			}
		});
		/*Close When Click Outside*/
		$body.on('click', function(e) {
			var $target = e.target;
			if (!jQuery($target).is('.pro-details-action') && !jQuery($target).parents().is('.pro-details-action') && $cartWrap.hasClass('show')) {
				$cartWrap.removeClass('show');
				$cartContent.removeClass('show');
			}
		});
	}



	// /*----------------------------
	// Cart Plus Minus Button
	// ------------------------------ */
	// var CartPlusMinus = jQuery('.cart-plus-minus');
	// CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
	// CartPlusMinus.append('<div class="inc qtybutton">+</div>');
	// jQuery(".qtybutton").on("click", function() {
	// 	var $button = jQuery(this);
	// 	var oldValue = $button.parent().find("input").val();
	// 	if ($button.text() === "+") {
	// 	v	ar newVal = parseFloat(oldValue) + 1;
	// 	} else {
	// 	// Don't allow decrementing below zero
	// 		if (oldValue > 0) {
	// 			var newVal = parseFloat(oldValue) - 1;
	// 		} else {
	// 			newVal = 1;
	// 		}
	// 	}
	// 	$button.parent().find("input").val(newVal);
	// });

	jQuery('#exampleModal').on('shown.bs.modal', function () {
		jQuery('.quickview-slide-active').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			fade: false,
			loop: true,
			dots: false,
			arrows: true,
			prevArrow: '<span class="icon-prev"><i class="icon-arrow-left"></i></span>',
			nextArrow: '<span class="icon-next"><i class="icon-arrow-right"></i></span>',
			responsive: [
				{
				breakpoint: 1199,
				settings: {
						slidesToShow: 3,
					}
				},
				{
				breakpoint: 991,
				settings: {
						slidesToShow: 3,
					}
				},
				{
				breakpoint: 767,
				settings: {
						slidesToShow: 3,
					}
				},
				{
				breakpoint: 575,
				settings: {
						slidesToShow: 2,
					}
				}
			]
		});
		jQuery('.quickview-slide-active a').on('click', function() {
			jQuery('.quickview-slide-active a').removeClass('active');
		})
	})

	/*====== Sidebar menu Active ======*/
	function mobileHeaderActive() {
		var navbarTrigger = jQuery('.mobile-header-button-active'),
		endTrigger = jQuery('.sidebar-close'),
		container = jQuery('.mobile-header-active'),
		wrapper4 = jQuery('.main-wrapper');

		wrapper4.prepend('<div class="body-overlay-1"></div>');

		navbarTrigger.on('click', function(e) {
			e.preventDefault();
			container.addClass('sidebar-visible');
			wrapper4.addClass('overlay-active-1');
		});

		endTrigger.on('click', function() {
			container.removeClass('sidebar-visible');
			wrapper4.removeClass('overlay-active-1');
		});

		$('.body-overlay-1').on('click', function() {
			container.removeClass('sidebar-visible');
			wrapper4.removeClass('overlay-active-1');
		});
	};
	mobileHeaderActive();

	/*---------------------
	mobile-menu
	--------------------- */
	var $offCanvasNav = jQuery('.mobile-menu , .category-menu-dropdown'),
	$offCanvasNavSubMenu = $offCanvasNav.find('.dropdown');

	/*Add Toggle Button With Off Canvas Sub Menu*/
	$offCanvasNavSubMenu.parent().prepend('<span class="menu-expand"><i></i></span>');

	/*Close Off Canvas Sub Menu*/
	$offCanvasNavSubMenu.slideUp();

	/*Category Sub Menu Toggle*/
	$offCanvasNav.on('click', 'li a, li .menu-expand', function(e) {
		var $this = jQuery(this);
		if ( ($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand')) ) {
			e.preventDefault();
			if ($this.siblings('ul:visible').length){
				$this.parent('li').removeClass('active');
				$this.siblings('ul').slideUp();
			} else {
				$this.parent('li').addClass('active');
				$this.closest('li').siblings('li').removeClass('active').find('li').removeClass('active');
				$this.closest('li').siblings('li').find('ul:visible').slideUp();
				$this.siblings('ul').slideDown();
			}
		}
	});


	/*--- checkout toggle function ----*/
	jQuery('.checkout-click1').on('click', function(e) {
		e.preventDefault();
		jQuery('.checkout-login-info').slideToggle(900);
	});


	/*--- checkout toggle function ----*/
	jQuery('.checkout-click3').on('click', function(e) {
		e.preventDefault();
		jQuery('.checkout-login-info3').slideToggle(1000);
	});

	/*-------------------------
	Create an account toggle
	--------------------------*/
	jQuery('.checkout-toggle2').on('click', function() {
		jQuery('.open-toggle2').slideToggle(1000);
	});

	jQuery('.checkout-toggle').on('click', function() {
		jQuery('.open-toggle').slideToggle(1000);
	});

	/*-------------------------
	checkout one click toggle function
	--------------------------*/
	var checked = jQuery( '.sin-payment input:checked' )
	if(checked){
		jQuery(checked).siblings( '.payment-box' ).slideDown(900);
	};
	jQuery( '.sin-payment input' ).on('change', function() {
		jQuery( '.payment-box' ).slideUp(900);
		jQuery(this).siblings( '.payment-box' ).slideToggle(900);
	});


	// Instantiate EasyZoom instances
	var $easyzoom = jQuery('.easyzoom').easyZoom();

	/*-------------------------------------
	Product details big image slider
	---------------------------------------*/

	/*--
	Magnific Popup
	------------------------*/
	jQuery('.img-popup').magnificPopup({
		type: 'image',
		gallery: {
			enabled: true
		}
	});



	/*------------------------
	Sidebar sticky active
	-------------------------- */
	jQuery('.sidebar-active').stickySidebar({
		topSpacing: 0,
		bottomSpacing: 30,
		minWidth: 767,
	});

	/*--- language currency active ----*/
	jQuery('.mobile-language-active').on('click', function(e) {
		e.preventDefault();
		jQuery('.lang-dropdown-active').slideToggle(900);
	});
	jQuery('.mobile-currency-active').on('click', function(e) {
		e.preventDefault();
		jQuery('.curr-dropdown-active').slideToggle(900);
	});


})();
